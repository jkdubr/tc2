//
//  TC2Tests.swift
//  TC2Tests
//
//  Created by Jakub Dubrovsky on 17/06/15.
//  Copyright (c) 2015 Ceska sporitelna. All rights reserved.
//

import UIKit
import XCTest

class TC2Tests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        XCTAssert(true, "Pass")
        
    }
    
    func testX1() {
        XCTAssertEqual("1", "1", "x")
    }
    
    func testX2() {
        XCTAssertEqual("1", "1", "x")
    }
    
    func testX3() {
        XCTAssertEqual("1", "1", "x")
    }
    
    func testX4() {
        XCTAssertEqual("1", "1", "x")
    }
    
    
    func testX5() {
        XCTAssertEqual("1", "1", "x")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }
    
    
}
